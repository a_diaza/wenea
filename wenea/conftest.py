import pytest
from rest_framework.test import APIClient

from wenea.users.models import User


@pytest.fixture(autouse=True)
def media_storage(settings, tmpdir):
    settings.MEDIA_ROOT = tmpdir.strpath


@pytest.fixture
def client() -> APIClient:
    return APIClient()
