from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.db.models.signals import pre_save
from django.dispatch import receiver

from wenea.charge_points.models import ChargePoint


@receiver(pre_save, sender=ChargePoint)
def notify_charge_point_status_change(sender, instance, **kwargs):
    if instance.id is not None:
        current = instance
        previous = ChargePoint.available_objects.get(id=instance.id)
        if previous.status != current.status:
            send_message(current, instance)


def send_message(current, instance):
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        'charge_point_status',
        get_message_to_send(current, instance)
    )


def get_message_to_send(current, instance):
    return {
        'type': 'on_new_status',
        'message': {
            "charge_point__id": instance.id,
            "status": current.status
        }
    }
