def get_data_from_factory(attributes, charge_point_factory):
    return {attribute: getattr(charge_point_factory, attribute) for attribute in attributes}
