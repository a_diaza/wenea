from datetime import datetime

import pytest
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

from wenea.charge_points.factories import ChargePointFactory
from wenea.charge_points.models import ChargePoint

pytestmark = pytest.mark.django_db


class TestListChargePoints:
    url = reverse("api:chargepoint-list")

    def test_list_charge_points_without_deleted(self, client: APIClient):
        ChargePointFactory()
        response = client.get(self.url, format='json')
        assert response.status_code == 200
        assert len(response.data) == ChargePoint.available_objects.count()

    def test_list_charge_points_with_deleted(self, client: APIClient):
        ChargePointFactory()
        ChargePointFactory(deleted_at=datetime.now())
        response = client.get(self.url, format='json')
        assert response.status_code == 200
        assert len(response.data) == ChargePoint.available_objects.count()
