from datetime import datetime

import pytest
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

from wenea.charge_points.factories import ChargePointFactory
from wenea.charge_points.models import ChargePoint

pytestmark = pytest.mark.django_db


class TestDeleteChargePoints:
    url_string = "api:chargepoint-detail"

    def test_delete_charge_point(self, client: APIClient):
        charge_point = ChargePointFactory()
        url = reverse(self.url_string, kwargs={"pk": charge_point.id})
        response = client.delete(url, format='json')
        assert response.status_code == 204
        assert not ChargePoint.available_objects.filter(id=charge_point.id).exists()
        assert ChargePoint.all_objects.filter(id=charge_point.id).exists()

    def test_delete_deleted_charge_point(self, client: APIClient):
        charge_point = ChargePointFactory(deleted_at=datetime.now())
        url = reverse(self.url_string, kwargs={"pk": charge_point.id})
        response = client.delete(url, format='json')
        assert response.status_code == 404
