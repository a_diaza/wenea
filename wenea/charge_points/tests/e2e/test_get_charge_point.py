from datetime import datetime

import pytest
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

from wenea.charge_points.factories import ChargePointFactory

pytestmark = pytest.mark.django_db


class TestGetChargePoints:

    url_string = "api:chargepoint-detail"

    def test_get_charge_point(self, client: APIClient):
        charge_point = ChargePointFactory()
        url = reverse(self.url_string, kwargs={"pk": charge_point.id})
        response = client.get(url, format='json')
        assert response.status_code == 200
        assert response.data['id'] == charge_point.id

    def test_get_deleted_charge_point(self, client: APIClient):
        charge_point = ChargePointFactory(deleted_at=datetime.now())
        url = reverse(self.url_string, kwargs={"pk": charge_point.id})
        response = client.get(url, format='json')
        assert response.status_code == 404
