import pytest
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

from wenea.charge_points.factories import ChargePointFactory
from wenea.charge_points.tests.utils import get_data_from_factory

pytestmark = pytest.mark.django_db


class TestUpdateChargePoints:

    url_string = "api:chargepoint-detail"
    attributes = ['name', 'status']

    def test_update_charge_point(self, client: APIClient):
        charge_point = ChargePointFactory()
        url = reverse(self.url_string, kwargs={"pk": charge_point.id})
        charge_point_factory = ChargePointFactory.build()
        data = get_data_from_factory(self.attributes, charge_point_factory)
        response = client.put(url, data=data, format='json')
        assert response.status_code == 200
        assert response.data['status'] == charge_point_factory.status
        assert response.data['name'] == charge_point_factory.name
