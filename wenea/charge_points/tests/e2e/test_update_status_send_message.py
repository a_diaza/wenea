import pytest
from asgiref.sync import sync_to_async
from channels.testing import WebsocketCommunicator

from wenea.charge_points.api.consumers import ChargePointStatusConsumer
from wenea.charge_points.factories import ChargePointFactory


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_update_status_send_message():
    app = ChargePointStatusConsumer()
    communicator = WebsocketCommunicator(app, "/testws/")
    connected, _ = await communicator.connect()
    assert connected

    charge_point = await sync_to_async(ChargePointFactory)(status='new')
    new_status = 'error'
    charge_point.status = new_status
    await sync_to_async(charge_point.save)()

    response = await communicator.receive_json_from()
    assert response == {'message': {'charge_point__id': charge_point.id, 'status': new_status}}
