import pytest
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

from wenea.charge_points.factories import ChargePointFactory
from wenea.charge_points.tests.utils import get_data_from_factory

pytestmark = pytest.mark.django_db


class TestCreateChargePoint:
    url = reverse("api:chargepoint-list")
    attributes = ['name', 'status']

    def test_create_charge_point_ok(self, client: APIClient):
        charge_point_factory = ChargePointFactory.build()
        data = get_data_from_factory(self.attributes, charge_point_factory)
        response = client.post(self.url, data, format='json')
        assert response.status_code == 201

    def test_create_charge_point_repeated_name(self, client: APIClient):
        charge_point_factory = ChargePointFactory()
        data = get_data_from_factory(self.attributes, charge_point_factory)
        response = client.post(self.url, data, format='json')
        assert response.status_code == 400
        assert 'name' in response.data

    def test_create_charge_point_invalid_status(self, client: APIClient):
        charge_point_factory = ChargePointFactory.build()
        data = get_data_from_factory(self.attributes, charge_point_factory)
        data['status'] = 'invalid_status'
        response = client.post(self.url, data, format='json')
        assert response.status_code == 400
        assert 'status' in response.data
