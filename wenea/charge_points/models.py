from django.db import models
from model_utils import Choices
from model_utils.models import StatusModel

from wenea.utils.models import SoftDeletableMixin


class ChargePoint(StatusModel, SoftDeletableMixin):

    STATUS = Choices('ready', 'charging', 'waiting', 'error')

    name = models.CharField(max_length=32, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
