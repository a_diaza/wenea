from factory import Faker, fuzzy
from factory.django import DjangoModelFactory

from wenea.charge_points.models import ChargePoint

CHARGE_POINTS_STATUSES = [x[0] for x in ChargePoint.STATUS]


class ChargePointFactory(DjangoModelFactory):
    class Meta:
        model = ChargePoint

    name = Faker('street_name')
    status = fuzzy.FuzzyChoice(CHARGE_POINTS_STATUSES)
