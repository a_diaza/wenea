import json

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer


class ChargePointStatusConsumer(WebsocketConsumer):
    group_name = "charge_point_status"

    def connect(self):
        async_to_sync(self.channel_layer.group_add)(
            self.group_name,
            self.channel_name
        )
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.group_name,
            self.channel_name
        )

    def receive(self, text_data=None, bytes_data=None):
        data = json.loads(text_data)
        message = data['message']
        async_to_sync(self.channel_layer.group_send)(
            self.group_name, {
                "type": 'on_new_status',
                "message": message
            }
        )

    def on_new_status(self, event):
        message = event['message']
        self.send(text_data=json.dumps({
            'message': message
        }))
