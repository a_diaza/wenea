from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from wenea.charge_points.api.serializers import ChargePointSerializer
from wenea.charge_points.models import ChargePoint


class ChargePointViewSet(mixins.CreateModelMixin, mixins.ListModelMixin, mixins.RetrieveModelMixin,
                         mixins.DestroyModelMixin, mixins.UpdateModelMixin, GenericViewSet):
    queryset = ChargePoint.available_objects.all()
    serializer_class = ChargePointSerializer
