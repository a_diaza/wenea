from rest_framework import serializers

from wenea.charge_points.models import ChargePoint


class ChargePointSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChargePoint
        fields = ('id', 'name', 'status', 'created_at', 'deleted_at',)
        read_only_fields = ('created_at', 'deleted_at',)
