from django.apps import AppConfig


class ChargePointConfig(AppConfig):
    name = 'wenea.charge_points'

    def ready(self):
        from . import signals
