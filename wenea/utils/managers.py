from datetime import datetime

from django.db.models import QuerySet, Manager


class SoftDeletableQuerySet(QuerySet):
    def delete(self):
        self.update(deleted_at=datetime.now())


class SoftDeletableManager(Manager):
    _queryset_class = SoftDeletableQuerySet

    def get_queryset(self):
        return self._queryset_class(model=self.model, using=self._db, hints=self._hints).filter(
            deleted_at__isnull=True
        )
