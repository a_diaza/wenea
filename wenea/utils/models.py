from datetime import datetime

from django.db import models

from wenea.utils.managers import SoftDeletableManager


class SoftDeletableMixin(models.Model):
    deleted_at = models.DateTimeField(null=True, blank=True)
    available_objects = SoftDeletableManager()
    all_objects = models.Manager()

    class Meta:
        abstract = True

    def delete(self, using=None, keep_parents=False):
        self.deleted_at = datetime.now()
        self.save(using=using)
