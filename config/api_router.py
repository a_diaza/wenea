from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter

from wenea.charge_points.api.views import ChargePointViewSet
from wenea.users.api.views import UserViewSet

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

#router.register("users", UserViewSet)
router.register("charge-points", ChargePointViewSet)


app_name = "api"
urlpatterns = router.urls
